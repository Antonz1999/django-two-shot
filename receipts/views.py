from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from receipts.models import Receipt, Account, ExpenseCategory
from receipts.forms import (
    CreateReceiptForm,
    CreateCategoryForm,
    CreateAccountForm,
)
from django.contrib.auth.decorators import login_required


@login_required
def show_receipts(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt_list,
    }
    return render(request, "receipts/receipt_list.html", context)


@login_required
def create_receipt(request):
    form = CreateReceiptForm()
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt_item = form.save(commit=False)
            receipt_item.purchaser = request.user
            receipt_item.save()
            return redirect("home")

    context = {
        "form": form,
    }

    return render(request, "receipts/create_receipt.html", context)


@login_required
def category_list(request):
    if request.method == "GET":
        expense_category = ExpenseCategory.objects.filter(owner=request.user)
        context = {
            "expense_category": expense_category,
        }
        return render(request, "categories/list.html", context)


@login_required
def account_list(request):
    if request.method == "GET":
        account_list = Account.objects.filter(owner=request.user)
        context = {
            "account_list": account_list,
        }
        return render(request, "accounts/list.html", context)


@login_required
def create_category(request):
    form = CreateCategoryForm()
    if request.method == "POST":
        form = CreateCategoryForm(request.POST)
        if form.is_valid():
            category_item = form.save(commit=False)
            category_item.owner = request.user
            category_item.save()
            return redirect("category_list")

    context = {
        "form": form,
    }

    return render(request, "categories/create.html", context)


@login_required
def create_account(request):
    form = CreateAccountForm()
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account_item = form.save(commit=False)
            account_item.owner = request.user
            account_item.save()
            return redirect("account_list")

    context = {
        "form": form,
    }

    return render(request, "accounts/create.html", context)
